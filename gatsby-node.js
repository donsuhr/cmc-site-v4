exports.onCreateBabelConfig = ({ stage, actions }) => {
  actions.setBabelPlugin({
    name: `@babel/plugin-proposal-class-properties`,
    stage,
    options: { loose: true },
  })
}
