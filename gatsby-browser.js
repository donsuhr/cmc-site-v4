import { polyfill as promisePolyfill } from "es6-promise"
import "whatwg-fetch"

promisePolyfill()
