import React from 'react'
import styles from './pill-button.module.scss'

class Component extends React.Component {
  render() {
    const { target, text, href, className } = this.props
    return (
      <a
        className={`${styles.button} ff-p-400 fs-16`}
        href={href}
        target={target || null}
        rel={target === `_blank` ? `noopener` : null}
      >
        {text}
      </a>
    )
  }
}

export default Component
