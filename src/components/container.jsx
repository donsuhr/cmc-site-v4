import React from 'react'
import 'modern-normalize/modern-normalize.css'
// import 'bootstrap/dist/css/bootstrap-grid.css'
import '../styles/global.css'
import '../styles/main.scss'
import '../styles/vendor/baseholdit-26-ff0000.css'

import SiteMetadata from './site-medadata'

export default ({ pathname, children }) => (
  <>
    <SiteMetadata pathname={pathname} />

    <div>{children}</div>
  </>
)
