import React from 'react'
import Img from 'gatsby-image'
import { StaticQuery, graphql } from 'gatsby'
import styles from './pattern-card.module.scss'

const FooImg = ({ imgProps }) => (
  <StaticQuery
    query={graphql`
      query {
        patternBoxImg: file(
          relativePath: { regex: "/resources-enter-btn-drk-blue/" }
        ) {
          childImageSharp {
            fixed(width: 130, height: 129) {
              ...GatsbyImageSharpFixed_withWebp_noBase64
            }
          }
        }
      }
    `}
    render={data => (
      <Img
        {...imgProps}
        fadeIn={false}
        fixed={data.patternBoxImg.childImageSharp.fixed}
      />
    )}
  />
)

const presets = {
  resources: {
    title: (
      <h2 className="ff-s fs-38 fs-md-45">
        Higher Ed Resources:
        <span className="ff-s fs-24">
          {' '}
          Find the Latest Insight and Thought Leadership
        </span>
      </h2>
    ),
    href: `/foo`,
    bgColorVarName: `bg-ptn--blue-bar-bkg-resources`,
  },
}

class Card extends React.Component {
  render() {
    let {
      preset,
      title,
      imgProps,
      bgColorVarName = `bgPtnBlueBarBkgResources`,
      href,
    } = this.props
    let img
    if (preset) {
      title = presets[preset].title
      href = presets[preset].href
      bgColorVarName = presets[preset].bgColorVarName
      imgProps = presets[preset].imgProps || {}
      img = <FooImg {...imgProps} />
    } else {
      img = <Img {...imgProps} />
    }

    return (
      <section className={bgColorVarName}>
        <div className={`${styles.component} pt-1 pb-1`}>
          <div className={styles.content}>
            <div className={`${styles.title} mb-1 mb-sm-0`}>{title}</div>
            <a href={href} className={`${styles.img}`}>
              {img}
            </a>
          </div>
        </div>
      </section>
    )
  }
}

export default Card
