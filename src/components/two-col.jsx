import React from 'react'
import styles from './two-col.module.scss'

class Component extends React.Component {
  render() {
    const { children, title, bgColorVarName = `white` } = this.props
    return (
      <section style={{ backgroundColor: `var(--${bgColorVarName})` }}>
        <div className={`${styles.component} pt-2 pb-1`}>
          <h2 className={`${styles.title} mt-0 mb-1 ff-s fs-28`}>{title}</h2>
          <div className="row">
            <div className={`${styles.content} col-sm`}>{children}</div>
          </div>
        </div>
      </section>
    )
  }
}

export default Component
