import React from 'react'
import PillButton from '../pill-button'
import styles from './item.module.scss'

const url = function(overrideUrl, group, s3url, id) {
  if (overrideUrl) {
    return overrideUrl
  }

  const langPrefix = group === `en-us` ? `` : `${group}/`
  return langPrefix === `pt-br/`
    ? `/solucoes/dl-case-study/${id}/${s3url}/`
    : `/${langPrefix}higher-education-resources/dl-case-study/${id}/${s3url}/`
}

const Item = ({ title, description, overrideUrl, group, s3url, id }) => (
  <div className={styles.container}>
    <p className={`${styles.header} ff-s fs-20`}>Case Study</p>
    <h3 className={`${styles.title} ff-s fs-20`}>{title}</h3>
    <div className={`${styles.content} ff-p fs-14`}>
      <p>{description}</p>
    </div>
    <div className={styles.linkWrapper}>
      <PillButton
        target="_blank"
        text="View"
        href={url(overrideUrl, group, s3url, id)}
      />
    </div>
  </div>
)

export default Item
