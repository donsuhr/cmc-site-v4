import React from 'react'
import styles from './pill-button-list.module.scss'
import PillButton from '../pill-button'

class Card extends React.Component {
  render() {
    const { links } = this.props
    return (
      <ul className={styles.list}>
        {links.map(function(link, i) {
          return (
            <li key={i} className={styles.item}>
              <PillButton {...link} />
            </li>
          )
        })}
      </ul>
    )
  }
}

export default Card
