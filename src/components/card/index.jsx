import React from 'react'
import Img from 'gatsby-image'
import styles from './card.module.scss'
import PillButtonList from '../pill-button-list'

class Card extends React.Component {
  render() {
    const {
      children,
      title,
      imgProps,
      links,
      bgColorVarName = `white`,
      imgColWidth = `3`,
      imgMaxWidth = `250px`,
      imgOnLeft = false,
      titleOverText = false,
    } = this.props
    imgProps.style = { ...imgProps.style, maxWidth: imgMaxWidth }
    const isFixed = Object.hasOwnProperty.call(imgProps, `fixed`)
    const imgColSelector = isFixed ? styles.isFixedImg : `col-sm-${imgColWidth}`
    const hasLinks = links && links.length > 0
    const pbStyle = hasLinks ? `pb-1` : `pb-2`

    return (
      <section style={{ backgroundColor: `var(--${bgColorVarName})` }}>
        <div className={`${styles.component} pt-2 ${pbStyle}`}>
          {!titleOverText && (
            <h2 className={`${styles.title} ff-s fs-28 mt-0 mb-1`}>{title}</h2>
          )}
          <div className={`${imgOnLeft ? styles.imgOnLeft : ``} row`}>
            <div className={`${styles.content} ff-p fs-16 col`}>
              {titleOverText && (
                <h2 className={`${styles.title} ff-s fs-28 mt-0 mb-1 `}>
                  {title}
                </h2>
              )}
              {children}
              {hasLinks && <PillButtonList links={links} />}
            </div>
            {/* col-sm-1 col-sm-2 col-sm-3 col-sm-4 col-sm-5 col-sm-6 */}
            <div className={`${styles.img} ${imgColSelector} `}>
              <Img {...imgProps} />
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default Card
