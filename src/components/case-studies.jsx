import React from 'react'
import styles from './case-studies.module.scss'
import config from '../config'
import Item from './case-studies/item'

const checkStatus = function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response
  }
  const error = new Error(response.statusText)
  error.response = response
  throw error
}

const parseJSON = function parseJSON(response) {
  return response.json()
}

function chunk(array, size) {
  return array.reduce((chunks, item, i) => {
    if (i % size === 0) {
      chunks.push([item])
    } else {
      chunks[chunks.length - 1].push(item)
    }
    return chunks
  }, [])
}

class Component extends React.Component {
  state = {
    data: [],
  }

  componentDidMount() {
    const { docType, site, group } = this.props

    const url = `${config.domains.api}/${docType}/${site}/${group}/`
    fetch(url)
      .then(checkStatus)
      .then(parseJSON)
      .then(result => {
        this.setState({ data: result.data })
      })
  }

  render() {
    const { data } = this.state
    const { group } = this.props
    const chunks = chunk(data, 3)
    return (
      <section>
        {chunks.map((row, chunkRowCounter) => {
          const rowBgColorClass =
            chunkRowCounter % 2 === 0
              ? `bgc--xs--box-bg-grey`
              : `bgc--xs--box-bg-alt`
          return (
            <div className={`${rowBgColorClass}`}>
              <div className={`${styles.component}`}>
                <div className={`  row`}>
                  {row.map((item, rowItemCounter) => {
                    const bgColorClass =
                      chunkRowCounter % 2 === 0
                        ? rowItemCounter % 2 === 0
                          ? `bgc--xs--box-bg-grey`
                          : `bgc--xs--box-bg-alt`
                        : rowItemCounter % 2 === 0
                        ? `bgc--xs--box-bg-alt`
                        : `bgc--xs--box-bg-grey`

                    return (
                      <div
                        key={rowItemCounter}
                        className={`${
                          styles.listItem
                        } ${bgColorClass} col-xs-12 col-sm-4`}
                      >
                        <Item
                          title={item.title}
                          description={item.description}
                          override-url={item[`override-url`]}
                          group={group}
                          id={item._id}
                          s3url={item.s3url}
                        />
                      </div>
                    )
                  })}
                </div>
              </div>
            </div>
          )
        })}
      </section>
    )
  }
}

export default Component
