import OpenSans300Woff from './open-sans-v16-latin-300.woff'
import OpenSans300Woff2 from './open-sans-v16-latin-300.woff2'
import OpenSans400Woff from './open-sans-v16-latin-regular.woff'
import OpenSans400Woff2 from './open-sans-v16-latin-regular.woff2'

import Oswald400Woff from './oswald-v17-latin-regular.woff'
import Oswald400Woff2 from './oswald-v17-latin-regular.woff2'

export {
  OpenSans300Woff,
  OpenSans300Woff2,
  OpenSans400Woff,
  OpenSans400Woff2,
  Oswald400Woff,
  Oswald400Woff2,
}
