const fontsPending = [
  `oswald-400-loaded`,
  `open-sans-400-loaded`,
  `open-sans-300-loaded`,
]

if (sessionStorage.getItem('fonts-loaded')) {
  // fonts cached, add class to document
  document.documentElement.classList.add(...fontsPending)
  fontsPending.splice(0, fontsPending.length)
}

function fontReducer(fontsArray) {
  return fontsArray.reduce((acc, font) => {
    const family = font.family.toLocaleLowerCase().replace(/\s/, `-`)
    const selector = `${family}-${font.weight}-loaded`
    if (fontsPending.indexOf(selector) !== -1) {
      fontsPending.splice(fontsPending.indexOf(selector), 1)
      acc.push(selector)
    }
    return acc
  }, [])
}

function checkFontsLoaded() {
  const fontsLoaded = Array.from(document.fonts)
  if (fontsPending.length && fontsLoaded.length) {
    const selectors = fontReducer(fontsLoaded)
    if (selectors.length) {
      document.documentElement.classList.add(...selectors)
    }
  }
}

if ('fonts' in document) {
  checkFontsLoaded()
  setTimeout(checkFontsLoaded, 150)
  document.fonts.onloadingdone = fontFaceSetEvent => {
    const selectors = fontReducer(Array.from(fontFaceSetEvent.target))
    if (selectors.length) {
      console.log('v2 selectors ', selectors)
      document.documentElement.classList.add(...selectors)
    }
    console.log(`v2 onloadingdone ${performance.now()} ${selectors}`)
    sessionStorage.setItem('fonts-loaded', true)
  }
} else {
  fontsPending.forEach(selector => {
    document.documentElement.classList.add(selector)
  })
}
