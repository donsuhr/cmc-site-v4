import React from 'react'
import { Helmet } from 'react-helmet'
import Container from '../components/container'
import PatternCard from '../components/pattern-card'
import CaseStudy from '../components/case-studies'

export default ({ data, location }) => {
  return (
    <Container pathname={location.pathname}>
      <Helmet>
        <meta
          name="Description"
          content="Blazing fast modern site generator for React. Go beyond static sites: build blogs, ecommerce sites, full-blown apps, and more with Gatsby."
        />
      </Helmet>
      <CaseStudy site="cmc" group="en-us" docType="case-studies" />
      <PatternCard preset="resources" />
    </Container>
  )
}
