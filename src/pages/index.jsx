import React from 'react'
import { Helmet } from 'react-helmet'
import { graphql } from 'gatsby'
import Container from '../components/container'
import Card from '../components/card'
import CardGrid from '../components/card-grid'
import TwoCol from '../components/two-col'
import PatternCard from '../components/pattern-card'

export default ({ data, location }) => {
  return (
    <Container pathname={location.pathname}>
      <Helmet>
        <meta
          name="Description"
          content="Blazing fast modern site generator for React. Go beyond static sites: build blogs, ecommerce sites, full-blown apps, and more with Gatsby."
        />
      </Helmet>

      <Card
        imgOnLeft
        titleOverText
        bgColorVarName="grey-2"
        title="Student Verification and Professional Judgments"
        links={[
          {
            text: `Learn More`,
            href: `http://www.google.com`,
          },
        ]}
        imgColWidth="4"
        imgProps={{
          fixed: data.verificationIcon.childImageSharp.fixed,
          fadeIn: false,
        }}
      >
        <p>
          The financial aid verification process is often a difficult and
          emotional experience for students. With our cloud-based verification
          software (formerly known as Education Partners Verification solution)
          you can automate and simplify the entire process for both students and
          staff.
        </p>
      </Card>

      <CardGrid
        title={
          <>
            We Focus on <span>Your Technology</span> so You Can Focus on
            Students
          </>
        }
        links={[
          {
            text: `Solution Sheet`,
            href: `http://www.google.com`,
            target: `_blank`,
          },
          {
            text: `Solution Sheet 2`,
            href: `http://www.google.com`,
          },
        ]}
        imgProps={{
          fluid: data.coverImage.childImageSharp.fluid,
          fadeIn: false,
        }}
      >
        <p>
          The cloud is no longer on some distant horizon for higher education.
          It’s here. In a climate of greater competition and reduced funding,
          today’s institutions need the cloud to focus more resources on the
          student and institutional lifecycle. They need the cloud to transform
          academic delivery, student success and operational efficiency on their
          terms, to evolve and grow dynamically.
        </p>

        <p>
          With CampusNexus Cloud, your institution gains rapid access to the
          most dynamic higher education solutions from Campus Management in a
          true SaaS model.
        </p>
      </CardGrid>

      <TwoCol bgColorVarName="grey-2" title="things nand stuff">
        <div>
          <h3>Access Innovation on Demand</h3>
          <p>
            Transform your institution on your terms. Leverage Campus
            Management’s solutions suite through CampusNexus Cloud.
          </p>
          <ul>
            <li>CampusNexus Student, CRM, Finance HR &amp; Payroll</li>
            <li>Subscription-based (Software-as-a-Service)</li>
            <li>Built on Microsoft Azure</li>
            <li>Rapidly deployed</li>
            <li>Expertly managed software delivery and support</li>
            <li>
              Optimally configured to your institution’s workflows and processes
            </li>
            <li>Instantly scalable as you grow and add locations</li>
            <li>Highest industry standards for security and uptime</li>
          </ul>
        </div>
        <div>
          <h3>Gain Peace of Mind; Maintain Control</h3>
          <p>
            With CampusNexus Cloud, you gain a reliable, secure and scalable
            platform for your most critical applications, without giving up the
            control you enjoyed on premises.
          </p>
          <ul>
            <li>Maintain control and scheduling of upgrades</li>
            <li>
              Leverage a flexible configuration and options to align the cloud
              platform with your institution’s workflows and needs
            </li>
            <li>
              Scale up or down dynamically as demand fluctuates throughout the
              academic year
            </li>
          </ul>
        </div>
        <div>
          <h3>Provide Strategic Leadership</h3>
          <ul>
            <li>
              Foster strategic use of technology and support your institution’s
              mission
            </li>
            <li>
              Partner with faculty and staff to transform teaching and learning
            </li>
            <li>
              {` `}
              Align technology initiatives with student success, outcomes and
              analytics-based decisions
            </li>
          </ul>
        </div>
        <div>
          <h3>Think Enterprise</h3>
          <ul>
            <li>Be agile and respond quickly to new initiatives on campus</li>
            <li>
              Enjoy peace of mind with built-in disaster recovery and security
            </li>
            <li>Add campuses and programs as needed</li>
            <li>
              Gain greater visibility through advanced analytics based on Big
              Data from across the institution
            </li>
          </ul>
        </div>
        <div>
          <h3>Optimize Resources</h3>
          <ul>
            <li>
              Support innovation and new initiatives with a highly flexible
              platform
            </li>
            <li>Augment staffing capacity without new hires</li>
            <li>Reduce risk and financial impact of big initial investments</li>
            <li>
              Retain staff by assigning meaningful, challenging work and
              offloading tactical tasks to CampusNexus Cloud
            </li>
          </ul>
        </div>
      </TwoCol>
      <Card
        title="CampusNexus Cloud Built on Microsoft Azure"
        imgProps={{ fixed: data.britelineImg.childImageSharp.fixed }}
        imgMaxWidth="255px"
      >
        <p>
          With a long and successful partnership, Campus Management and
          Microsoft provide your institution a proven foundation for higher
          education solutions in the cloud. With Azure, you get the freedom to
          quickly build and deploy the CampusNexus suite as well as 3rd party or
          proprietary solutions, using the tools, applications, and frameworks
          of your choice.
        </p>

        <p>
          CampusNexus Cloud’s security procedures are certified to the
          international ISO27001 security management standard and is
          independently audited on an annual basis. Microsoft Azure complies
          with both international and industry-specific compliance standards and
          participates in rigorous third-party audits. Learn more at the Azure
          Trust Center .
        </p>
      </Card>

      <PatternCard
        title={
          <h2 className="ff-s fs-24">
            Ready to{' '}
            <span className="fs-38 fs-md-45">Transform Your Institution?</span>{' '}
            Let’s Discuss Your Challenges and Goals.
          </h2>
        }
        href="/foo"
        imgProps={{
          fixed: data.patternBoxImg.childImageSharp.fixed,
          fadeIn: false,
        }}
        bgColorVarName="bg-ptn--blue-bar-bkg-nexus"
      />
      <PatternCard
        title={
          <h2 className="ff-s fs-38 fs-md-45">
            Higher Ed Resources:{' '}
            <span className="fs-24">
              Find the Latest Insight and Thought Leadership
            </span>
          </h2>
        }
        href="/foo"
        imgProps={{
          fixed: data.patternBoxImg.childImageSharp.fixed,
          fadeIn: false,
        }}
        bgColorVarName="bg-ptn--gold-bar-bkg-insights"
      />
      <PatternCard preset="resources" />
    </Container>
  )
}

export const query = graphql`
  query {
    coverImage: file(relativePath: { regex: "/services-cloud-woman/" }) {
      childImageSharp {
        fluid(maxWidth: 250, quality: 90) {
          ...GatsbyImageSharpFluid_withWebp_noBase64
        }
      }
    }
    britelineImg: file(relativePath: { regex: "/Bright/" }) {
      childImageSharp {
        fixed(width: 225, height: 76) {
          ...GatsbyImageSharpFixed_withWebp_noBase64
        }
      }
    }
    verificationIcon: file(relativePath: { regex: "/verification-icon/" }) {
      childImageSharp {
        fixed(width: 152, height: 152) {
          ...GatsbyImageSharpFixed_withWebp_noBase64
        }
      }
    }
    patternBoxImg: file(relativePath: { regex: "/insights-enter-btn-gold/" }) {
      childImageSharp {
        fixed(width: 109, height: 127) {
          ...GatsbyImageSharpFixed_withWebp_noBase64
        }
      }
    }
  }
`
