const path = require(`path`)

module.exports = {
  siteMetadata: {
    title: `Campus Management Corp®`,
    siteUrl: `https://www.campusmanagement.com`,
    description: `Blazing fast modern site generator for React`,
    twitter: `@CampusMgmt`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: path.join(__dirname, `src`, `images`),
      },
    },
    {
      resolve: `gatsby-plugin-sass`,
      options: {
        precision: 8,
      },
    },
    `gatsby-plugin-postcss`,
    {
      resolve: `gatsby-plugin-purgecss`,
      options: {
        printRejected: true, // Print removed selectors and processed file names
        develop: false, // Enable while using `gatsby develop`
        // tailwind: true, // Enable tailwindcss support
        whitelist: [`oswald-400-loaded`], // Don't remove this selector
        ignore: [`/modern-normalize.css`, `/src/styles/global.module.css`],
        // purgeOnly : ['components/', '/main.css', 'bootstrap/'], // Purge only these files/folders
      },
    },
  ],
}
